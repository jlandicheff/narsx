# NARSX

Narsx is a CodeMirror addon allowing two users to edit one single text remotely, and in real time.
The connection is made from peer to peer, meaning no server is required for the exchange (although
a server might be practical to bind the users)

## INSTALLATION

Follow the CodeMirror guide to setup a CodeMirror editor on your webpage, then import:
	- lib/peer.js
	- lib/diff_match_patch.js
	- addon/narsx/editor.js
	- addon/narsx/connection.js
	- addon/narsx/narsx.js

In a script, once the window is loaded, call one of the two functions, depending on whether
the user is the master of the client of the exchange:

	- initNarsxMasterSession()
	- initNarsxClientSession(masterTokenGivenByPeerJS)

In addon/narsx/narsx.js, at the top of the file, define your own registerToken() function.
This function allows you to deal with the registration of the token retrieved by Master
(for example, by saving it in a database). 

## TESTING

Tests are executed by jasmine, and wrapped in a Testme framework (in order to test code requiring a window object)

To execute them, install both packages:
	- npm install jasmine
	- npm install testme

Run 'testme' in the root folder of the project.
Open a browser and navigate to http://localhost:7357/
The results appear both in the console and in the browser.