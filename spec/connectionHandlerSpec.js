describe('ConnectionHandler', function() {
    it('sets the callback for receiving updates', function() {
    	var cb = function(oi) {
    		console.log(oi);
    	};

        var ch = new ConnectionHandler(cb);
        expect(ch.onUpdateCallback).toBe(cb);
    });

    it('routes received data to the appropriate callback for packet SYNC', function() {
        var a = {};
        var ch = new MasterConnectionHandler(function(){}, function(){a['test'] = 'test';}, function(){});

        ch._receiveData({typePackage: 'SYNC'});
        expect(a['test']).toBe('test');
    });

    it('routes received data to the appropriate callback for packet UPDATE as master', function() {
        var a = {};
        var ch = new MasterConnectionHandler(
            function(){},
            function(){a['test'] = 'test';},
            function(){a['test'] = 'otherTest'}
        );

        ch._receiveData({typePackage: 'UPDATE'});
        expect(a['test']).toBe('otherTest');
    });

    it('routes received data to the appropriate callback for packet RSYNC', function() {
        var a = {};
        var ch = new ClientConnectionHandler(
            function(){},
            function(){a['test'] = 'test';},
            function(){a['test'] = 'otherTest'}
        );

        ch._receiveData({typePackage: 'RSYNC'});
        expect(a['test']).toBe('test');
    });

    it('routes received data to the appropriate callback for packet UPDATE as client', function() {
        var a = {};
        var ch = new ClientConnectionHandler(
            function(){},
            function(){a['test'] = 'test';},
            function(){a['test'] = 'otherTest'}
        );

        ch._receiveData({typePackage: 'UPDATE'});
        expect(a['test']).toBe('otherTest');
    });
});